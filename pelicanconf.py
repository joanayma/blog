#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'joanayma'
SITENAME = "Joan's blog"
SITEURL = 'https://joan.ayma.cat'

RESUMEURL = 'https://cv.joan.ayma.cat'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = u'ca'

THEME = 'pelican-blue'

TWITTER_USERNAME = 'joanayma'

DISPLAY_PAGES_ON_MENU = True

MENUITEMS = (('Blog', SITEURL), ('Resume', RESUMEURL),)

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
