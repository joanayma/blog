Title: Pelican on GitLab Pages!
Date: 2018-02-08
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

## New site!

Started new portfolio and blog with pelican and gitlab pages!

The source code of this site is at <https://gitlab.com/pages/pelican>. Learn about GitLab Pages at <https://pages.gitlab.io>.

## TODO:
 - gitlab pages custom domain, locked for gitlab 10.5.
 - New profile pic.
 - New posts!
 - Improve the resume.

Thanks for watching.
