FROM python:2.7-alpine
COPY . build
WORKDIR build
RUN pip install -r requirements.txt && \
    pelican -Ds publishconf.py
